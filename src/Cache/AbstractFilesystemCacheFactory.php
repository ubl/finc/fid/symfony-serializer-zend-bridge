<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author  Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\SymfonySerializerZendBridge\Cache;

use Psr\Container\ContainerInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

abstract class AbstractFilesystemCacheFactory
{
    abstract protected function getName(): string;

    public function __invoke(ContainerInterface $container)
    {
        $name = $this->getName();
        $config = $container->get('config')['symfony_serializer'] ?? [];
        $cacheConfig = $config['caches'][$name] ?? [];
    
        if (getenv('SERIALIZER_BRIDGE_CACHE_DIR')) {
            $directory = getenv('SERIALIZER_BRIDGE_CACHE_DIR');
        } else {
            $directory = $cacheConfig['directory'] ?? $config['cache_directory'] ?? '/var/cache';
        }

        $lifetime = $cacheConfig['lifetime'] ?? $config['cache_lifetime'] ?? 0;
        $namespace = $cacheConfig['namespace'] ?? md5($name);

        if (!file_exists($directory)) {
            mkdir($directory);
        }

        return new FilesystemAdapter($namespace, $lifetime, $directory);
    }
}