<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author  Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\SymfonySerializerZendBridge;

use finc\SymfonySerializerZendBridge\Encoder\EncoderManager;
use finc\SymfonySerializerZendBridge\Normalizer\NormalizerManager;
use finc\SymfonySerializerZendBridge\Normalizer\SnakeCaseObjectNormalizer;
use finc\SymfonySerializerZendBridge\Normalizer\XmlAttributesDenormalizer;
use Psr\Container\ContainerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SerializerFactory
{
    protected $config
        = [
            'normalizers' => [
                ArrayDenormalizer::class         => 400,
                XmlAttributesDenormalizer::class => 300,
                SnakeCaseObjectNormalizer::class => 200,
                ObjectNormalizer::class          => 100,
            ],
            'encoders'    => [
                JsonEncoder::class => 200,
                XmlEncoder::class  => 100,
            ],
        ];

    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config')['symfony_serializer'] ?? [];
        $config = array_replace_recursive($this->config, $config);

        arsort($config['encoders']);
        arsort($config['normalizers']);

        $encoders = array_keys($config['encoders']);
        $normalizers = array_keys($config['normalizers']);

        $encoderManager = $container->get(EncoderManager::class);
        $normalizerManager = $container->get(NormalizerManager::class);

        $encoders = array_map([$encoderManager, 'get'], $encoders);
        $normalizers = array_map([$normalizerManager, 'get'], $normalizers);

        return new Serializer($normalizers, $encoders);
    }
}