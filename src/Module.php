<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author  Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\SymfonySerializerZendBridge;

use finc\SymfonySerializerZendBridge\Accessor\AccessorManager;
use finc\SymfonySerializerZendBridge\Accessor\AccessorManagerFactory;
use finc\SymfonySerializerZendBridge\Cache\CacheManager;
use finc\SymfonySerializerZendBridge\Cache\CacheManagerFactory;
use finc\SymfonySerializerZendBridge\Encoder\EncoderManager;
use finc\SymfonySerializerZendBridge\Encoder\EncoderManagerFactory;
use finc\SymfonySerializerZendBridge\Extractor\ExtractorManager;
use finc\SymfonySerializerZendBridge\Extractor\ExtractorManagerFactory;
use finc\SymfonySerializerZendBridge\Loader\LoaderManager;
use finc\SymfonySerializerZendBridge\Loader\LoaderManagerFactory;
use finc\SymfonySerializerZendBridge\Metadata\MetadataManager;
use finc\SymfonySerializerZendBridge\Metadata\MetadataManagerFactory;
use finc\SymfonySerializerZendBridge\NameConverter\NameConverterManager;
use finc\SymfonySerializerZendBridge\NameConverter\NameConverterManagerFactory;
use finc\SymfonySerializerZendBridge\Normalizer\NormalizerManager;
use finc\SymfonySerializerZendBridge\Normalizer\NormalizerManagerFactory;
use Symfony\Component\Serializer\SerializerInterface;

class Module
{
    public function getConfig()
    {
        return [
            'service_manager' => [
                'factories' => [
                    AccessorManager::class      => AccessorManagerFactory::class,
                    CacheManager::class         => CacheManagerFactory::class,
                    EncoderManager::class       => EncoderManagerFactory::class,
                    ExtractorManager::class     => ExtractorManagerFactory::class,
                    LoaderManager::class        => LoaderManagerFactory::class,
                    MetadataManager::class      => MetadataManagerFactory::class,
                    NameConverterManager::class => NameConverterManagerFactory::class,
                    NormalizerManager::class    => NormalizerManagerFactory::class,
                    SerializerInterface::class  => SerializerFactory::class,
                ],
            ],
        ];
    }
}