<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author  Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\SymfonySerializerZendBridge\Normalizer;

use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;

class OptionalObjectNormalizer implements ContextAwareDenormalizerInterface,
    ContextAwareNormalizerInterface, SerializerAwareInterface
{
    /**
     * @var ObjectNormalizer
     */
    protected $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    public function denormalize(
        $data,
        $class,
        $format = null,
        array $context = []
    ) {
        return $this->objectNormalizer->denormalize($data, $class, $format,
            $context);
    }

    public function normalize($object, $format = null, array $context = [])
    {
        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    public function supportsDenormalization(
        $data,
        $type,
        $format = null,
        array $context = []
    ) {
        return ($context[static::class] ?? false)
            && $this->objectNormalizer
                ->supportsDenormalization($data, $type, $format);
    }

    public function supportsNormalization(
        $data,
        $format = null,
        array $context = []
    ) {
        return ($context[static::class] ?? false)
            && $this->objectNormalizer->supportsNormalization($data, $format);
    }

    public function setSerializer(SerializerInterface $serializer)
    {
        $this->objectNormalizer->setSerializer($serializer);
    }
}