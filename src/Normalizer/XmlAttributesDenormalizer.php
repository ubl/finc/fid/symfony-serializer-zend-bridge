<?php
/**
 * Copyright (C) 2018 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\SymfonySerializerZendBridge\Normalizer;

use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;

class XmlAttributesDenormalizer implements ContextAwareDenormalizerInterface,
    DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    public function supportsDenormalization(
        $data,
        $type,
        $format = null,
        array $context = []
    ) {
        return $format === 'xml' && ($context[self::class] ?? false);
    }

    public function denormalize(
        $data,
        $class,
        $format = null,
        array $context = []
    ) {
        return $this->denormalizer->denormalize($this->mapData($data),
            $class, $format, [self::class => false] + $context);
    }

    protected function mapData(array $data)
    {
        $result = $data;

        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $result[$key] = $this->mapData($value);
            } elseif (is_string($key) && strlen($key)) {
                if ($key[0] === '@') {
                    $result[substr($key, 1)] = $value;
                } elseif ($key == '#') {
                    $result['text'] = $value;
                } else {
                    $result[$key] = $value;
                }
            } else {
                $result[$key] = $value;
            }
        }

        return $result;
    }
}