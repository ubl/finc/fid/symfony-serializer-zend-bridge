<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author  Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license http://opensource.org/licenses/gpl-2.0.php NU GPLv2
 */

namespace finc\SymfonySerializerZendBridge\Normalizer;

use finc\SymfonySerializerZendBridge\Accessor\AccessorManager;
use finc\SymfonySerializerZendBridge\Extractor\ExtractorManager;
use finc\SymfonySerializerZendBridge\Metadata\MetadataManager;
use finc\SymfonySerializerZendBridge\NameConverter\NameConverterManager;
use Psr\Container\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ObjectNormalizerFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return ObjectNormalizer
     */
    public function __invoke(ContainerInterface $container)
    {
        $metadataFactory = $this->getClassMetadataFactory($container);
        $nameConverter = $this->getNameConverter($container);
        $propertyAccessor = $this->getPropertyAccessor($container);
        $propertyExtractor = $this->getPropertyTypeExtractor($container);

        return new ObjectNormalizer($metadataFactory, $nameConverter,
            $propertyAccessor, $propertyExtractor);
    }

    protected function getClassMetadataFactory(
        ContainerInterface $container
    ): ClassMetadataFactoryInterface {
        return $container->get(MetadataManager::class)
            ->get(ClassMetadataFactoryInterface::class);
    }

    protected function getNameConverter(
        ContainerInterface $container
    ): ?NameConverterInterface {
        return $container->get(NameConverterManager::class)
            ->get(NameConverterInterface::class);
    }

    protected function getPropertyAccessor(
        ContainerInterface $container
    ): ?PropertyAccessorInterface {
        return $container->get(AccessorManager::class)
            ->get(PropertyAccessorInterface::class);
    }

    protected function getPropertyTypeExtractor(
        ContainerInterface $container
    ): PropertyTypeExtractorInterface {
        return $container->get(ExtractorManager::class)
            ->get(PropertyTypeExtractorInterface::class);
    }
}