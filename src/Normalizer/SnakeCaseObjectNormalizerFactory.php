<?php

namespace finc\SymfonySerializerZendBridge\Normalizer;

use finc\SymfonySerializerZendBridge\NameConverter\NameConverterManager;
use Psr\Container\ContainerInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class SnakeCaseObjectNormalizerFactory extends ObjectNormalizerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $objectNormalizer = parent::__invoke($container);

        return new SnakeCaseObjectNormalizer($objectNormalizer);
    }

    protected function getNameConverter(
        ContainerInterface $container
    ): ?NameConverterInterface {
        return $container->get(NameConverterManager::class)
            ->get(CamelCaseToSnakeCaseNameConverter::class);
    }
}